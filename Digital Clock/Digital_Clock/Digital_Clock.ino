/*
 * LCD
 */
#include <LiquidCrystal.h>

// select the pins used on the LCD panel
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

// read the buttons
int read_LCD_buttons() {
  adc_key_in = analogRead(0);      // read the value from the sensor
  if (adc_key_in > 1000) return btnNONE;
  if (adc_key_in < 100) return btnRIGHT;
  if (adc_key_in < 200)  return btnUP;
  if (adc_key_in < 400)  return btnDOWN;
  if (adc_key_in < 600)  return btnLEFT;
  if (adc_key_in < 800)  return btnSELECT;
  
  return btnNONE;  // when all others fail, return this...
}

/*
 * Clock
 */
int hours = 12;
int minutes = 0;
int seconds = 0;
char meridiem = 'A';

void printTime() {
  lcd.setCursor(0, 0);
  char clockTime[11];
  sprintf(clockTime, "%02i:%02i:%02i %cM", hours, minutes, seconds, meridiem);
  Serial.println(clockTime);
  lcd.print(String(clockTime));
}

void calculateTime() {
  seconds += 1;
  if (seconds == 60) {
    seconds = 0;
    minutes += 1;
    if (minutes == 60) {
      minutes = 0;
      hours += 1;
      if (hours == 12) {
        if (meridiem == 'A') {
          meridiem = 'P';
        } else {
          meridiem = 'A';
        }
      } else if (hours == 13) {
        hours = 1;
      }
    }
  }
}

/*
 * set mode
 */
bool setMode = false;
int component = 1;
int cursorPlace = 0;
void nextComponent() {
  component += 1;
  if (component == 5) {
    component = 4;
  }
  cursorPlace = 3 * (component - 1);
  removeCursor();
  displayCursor();
}
void previousComponent() {
  component -= 1;
  if (component == 0) {
    component = 1;
  }
  cursorPlace = 3 * (component - 1);
  removeCursor();
  displayCursor();
}
void setComponent(char state) {
  if (component == 4) {
      if (meridiem == 'A') {
         meridiem = 'P';
      } else {
         meridiem = 'A';
      }
  } else {
    switch (component) {
      case 1: {
        if (state == 'u') {
          increase('h');
        } else if (state == 'd') {
          decrease('h');
        }
        break;
      }
      case 2: {
        if (state == 'u') {
          increase('m');
        } else if (state == 'd') {
          decrease('m');
        }
        break;
      }
      case 3: {
        if (state == 'u') {
          increase('s');
        } else if (state == 'd') {
          decrease('s');
        }
        break;
      }
    }
  }
  printTime();
}
void increase(char comp) {
  if (comp == 'h') {
    hours += 1;
    if (hours == 13) {
      hours = 1;
    }
  } else if (comp == 'm') {
    minutes += 1;
    if (minutes == 60) {
      minutes = 0;
    }
  } else if (comp == 's') {
    seconds += 1;
    if (seconds == 60) {
      seconds = 0;
    }
  }
}
void decrease(char comp) {
  if (comp == 'h') {
    hours -= 1;
    if (hours == 0) {
      hours = 12;
    }
  } else if (comp == 'm') {
    minutes -= 1;
    if (minutes == -1) {
      minutes = 59;
    }
  } else if (comp == 's') {
    seconds -= 1;
    if (seconds == -1) {
      seconds = 59;
    }
  }
}
void displayCursor() {
  lcd.setCursor(cursorPlace, 1);
  lcd.print("--");
}
void removeCursor() {
  lcd.setCursor(0, 1);
  lcd.print("           ");
}

/*
 * setup
 */
void setup() {
  Serial.begin(9600);

  // start the library
  lcd.begin(16, 2);

  // start the clock
  printTime();
}

/*
 * logic
 */
void loop() {
  if (setMode) {
    lcd_key = read_LCD_buttons();
    switch (lcd_key) {
      case btnRIGHT: {
          nextComponent();
          break;
      }
      case btnLEFT: {
          previousComponent();
          break;
      }
      case btnUP: {
          setComponent('u');
          break;
      }
      case btnDOWN: {
          setComponent('d');
          break;
      }
      case btnSELECT: {
          component = 1;
          cursorPlace = 0;
          removeCursor();
          setMode = false;
          break;
      }
      case btnNONE: {
          break;
      }
    }
    delay(300);
  } else {
    calculateTime();
    printTime();
    // read the buttons
    lcd_key = read_LCD_buttons();
    if (lcd_key == btnSELECT) {
      setMode = true;
      delay(200);
      displayCursor();
    } else {
      delay(1000);
    }
  }
}
